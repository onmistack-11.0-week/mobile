import 'intl'
import 'intl/locale-data/jsonp/pt-BR'

import React from 'react';

import Routes from './src/routes'

export default function App() {
  return <Routes />
}

/*

Iniciar projeto:

1º instale o expo na máquina:
(https://docs.expo.io/versions/v36.0.0/get-started/installation/)
sudo npm i -g expo-cli
obs: o que é o expo? ele vai ajudar a executar o projeto diretamento em um celular,
sem precisar de um emulador

2º crie um projeto com o expo:
expo init mobile
obs: um equivalente do react-create-app

3º execute o projeto com o 'yarn start' e tenho o app 'Expo' instalado no celular.
Após iniciar o projeto, scanei o QR Code e aperte no navegador 'Run on [device]' para o projeto funcionar
no device selecionado.
obs: a opção para web funciona muito bem também!

*/

/**

* Gerando o APK com expo:
* https://www.youtube.com/watch?v=wYMvzbfBdYI

**/