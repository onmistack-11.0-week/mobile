import axios from 'axios'
import config from '../configfile'

const api = axios.create({
  baseURL: config[process.env.NODE_ENV] ? config[process.env.NODE_ENV].api_url : 'http://192.168.0.3:3333'
})

export default api