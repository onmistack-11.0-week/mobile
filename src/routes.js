// Reference:
// https://docs.expo.io/versions/v36.0.0/guides/routing-and-navigation/

// install command: 
//    npm install @react-navigation/native
// install dependencies into Expo command: 
//    expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view

// Example: https://reactnavigation.org/docs/hello-react-navigation
// install a simple navigation mode: npm install @react-navigation/stack

import React from 'react'

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import Incidents from './pages/Incidents'
import Detail from './pages/Detail'

const AppStack = createStackNavigator()

export default function Routes() {
  return (
    <NavigationContainer>

      <AppStack.Navigator screenOptions={{ headerShown: false }}>
        <AppStack.Screen name="Incidents" component={Incidents} />
        <AppStack.Screen name="Detail" component={Detail} />
      </AppStack.Navigator>

    </NavigationContainer>
  )
}