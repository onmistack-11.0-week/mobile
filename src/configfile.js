export default {
  development: {
    api_url: 'http://192.168.0.3:3333/'
  },
  production: {
    api_url: 'https://gdev-be-the-hero.herokuapp.com/'
  }
}